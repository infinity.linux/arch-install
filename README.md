# Arch-Install

This provides an installation guide along with scripts to make installing Arch with KDE easier.

These are in house scripts and guides that are intended to be modified for your specific needs.

So please DO NOT just run them and hope for the best.

My general assumptions are:
1. One Disk
2. Fresh Install
3. Desire for Timeshift/BTRFS

If you don't like these assumptions the guide and scripts are set up to be easily edited to fit your needs.

Please proceed to the instal-guide to begin installation.
