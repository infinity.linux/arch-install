#!/bin/bash

#------------------------------------------------------------------------------------------------#
#                                                                                                #
#  Purpose:                                                                                      #
#    - Automate the steps for installing KDE on Arch Linux.                                      #
#                                                                                                #
#------------------------------------------------------------------------------------------------#

echo -e "\n************************************************************************************************************\n"

# Set ntp, enable hwclock, and enable timesyncd.
sudo timedatectl set-ntp true
sudo hwclock --systohc
sudo systemctl enable systemd-timesyncd

# Set up the firewall.
sudo firewall-cmd --add-port=1025-65535/tcp --permanent
sudo firewall-cmd --add-port=1025-65535/udp --permanent
sudo firewall-cmd --reload


# Install the required desktop packages.
sudo pacman -S plasma-meta kde-accessibility-meta kde-graphics-meta kde-multimedia-meta kde-network-meta kde-pim-meta kde-sdk-meta kde-system-meta kde-utilities-meta

# Install paru and the aur packages.
git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si
paru -S brave-bin timeshift-bin timeshift-autosnap

sudo systemctl enable sddm
sleep 10

echo -e "All steps in .kde.sh are complete. Please refer back to the install-guide."
echo -e "Starting at the !!!FROM KDE!!! comment."

echo -e "\n************************************************************************************************************\n"
