#!/bin/bash

#------------------------------------------------------------------------------------------------#
#                                                                                                #
#  Purpose:                                                                                      #
#    - Automate the steps for installing Arch Linux.                                             #
#                                                                                                #
#------------------------------------------------------------------------------------------------#

echo -e "\n************************************************************************************************************\n"

# Set timezone and enable hwclock.
ln -sf /usr/share/zoneinfo/America/New_York /etc/localtime
hwclock --systohc

# Generate the locales.
sed -i '177s/.//' /etc/locale.gen
locale-gen

# Set language and keyboard.
echo "LANG=en_US.UTF-8" >> /etc/locale.conf
echo "KEYMAP=us" >> /etc/vconsole.conf

# Set hostname and enable hosts file. *Set desired hostname*
echo "arch" >> /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 arch.localdomain arch" >> /etc/hosts

# Change root password. !Set your own password!
echo root:password | chpasswd

pacman -S acpi acpi_call acpid alsa-utils avahi bluez bluez-utils bridge-utils cups dialog dnsmasq dnsutils dosfstools edk2-ovmf efibootmgr firewalld flatpak grub grub-btrfs gvfs gvfs-smb hplip inetutils ipset iptables-nft linux-docs linux-headers meld mtools networkmanager network-manager-applet nfs-utils nss-mdns ntfs-3g openbsd-netcat openssh os-prober packagekit-qt5 pipewire pipewire-alsa pipewire-jack pipewire-pulse qemu qemu-arch-extra reflector rsync sof-firmware terminus-font vde2 virt-manager wpa_supplicant xdg-user-dirs xdg-utils xorg

# If installing on a lptop install tlp for battery monitoring.
# pacman -S tlp

# Select what GPU packages you want.
# pacman -S xf86-video-amdgpu
pacman -S nvidia-dkms nvidia-settings nvidia-utils

# Install GRUB and generate config.
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB
grub-mkconfig -o /boot/grub/grub.cfg

# Enable system services.
systemctl enable NetworkManager
systemctl enable bluetooth
systemctl enable cups.service
systemctl enable sshd
systemctl enable avahi-daemon
# You can comment out the below command if you didn't install the tlp package.
#systemctl enable tlp
systemctl enable reflector.timer
systemctl enable fstrim.timer
systemctl enable libvirtd
systemctl enable firewalld
systemctl enable acpid

# Create user, change password, and create sudoers file. !Set your own password!
useradd -m -g users -G wheel auser
echo auser:password | chpasswd
usermod -aG libvirt auser


echo -e "All steps in .base.sh are complete. Please refer back to the install-guide."
echo -e "Starting at the !!!FROM BASE!!! comment."

echo -e "\n************************************************************************************************************\n"
